package com.epam.java.grow.event;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import rx.Observable;
import rx.schedulers.Timestamped;

import static com.diabolicallabs.vertx.cron.CronObservable.cronspec;
import static io.vertx.rx.java.RxHelper.scheduler;

public class CronScheduler extends AbstractVerticle {

  public CronScheduler(Vertx vertx) {
    this.vertx = vertx;
  }

  public static void main(String[] args) throws Exception {
    VertxOptions vertxOptions = new VertxOptions();
    Vertx.clusteredVertx(vertxOptions,  res -> {
      if (res.succeeded()) new CronScheduler(res.result()).start();
			else res.cause().printStackTrace();
    });
  }

	@Override
	public void start() {
		on("* */1 * * * ?")
				.subscribe(
						onNext -> vertx.eventBus().publish("root", "A Message!"),
						onError -> System.out.println("onError = " + onError.getMessage()),
						() -> System.out.println("complete 1")
				);
	}

  private Observable<Timestamped<Long>> on(String cronExpression) {
    return cronspec(scheduler(vertx), cronExpression);
  }
}
