package com.epam.java.grow.edfsm.machine;

import com.epam.java.grow.edfsm.FiniteStateMachine;
import com.epam.java.grow.edfsm.State;
import io.vertx.core.eventbus.EventBus;

public class GoToWord<T> {

	private EventBus eventBus;

	public GoToWord(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public OnWord<T> on(String event) {
		return new OnWord<>(eventBus, event);
	}

	public FromWord<T> from(State state) {
		return new FromWord<>(eventBus);
	}

	public FiniteStateMachine<T> build() {
		return new FiniteStateMachine<>();
	}
}
