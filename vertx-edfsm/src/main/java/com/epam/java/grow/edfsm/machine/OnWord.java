package com.epam.java.grow.edfsm.machine;

import com.epam.java.grow.edfsm.State;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;

public class OnWord<T> {

	private EventBus eventBus;
	private String event;

	public OnWord(EventBus eventBus, String event) {
		this.eventBus = eventBus;
		this.event = event;
	}

	public GoToWord<T> goTo(State<T> state) {
		return new GoToWord<>(eventBus);
	}

	public PerformWord<T> perform(Handler<Message<T>> action) {
		eventBus.consumer(event, action);
		return new PerformWord<>(eventBus);
	}
}
