package com.epam.java.grow.edfsm.machine;

import com.epam.java.grow.edfsm.State;
import io.vertx.core.eventbus.EventBus;

public class PerformWord<T> {

	private EventBus eventBus;

	public PerformWord(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public GoToWord<T> goTo(State<T> state) {
		return new GoToWord<>(eventBus);
	}
}
