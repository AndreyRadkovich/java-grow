package com.epam.java.grow.edfsm.machine;

import io.vertx.core.eventbus.EventBus;

public class FromWord<T> {

	private EventBus eventBus;

	public FromWord(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public OnWord<T> on(String event) {
		return new OnWord<>(eventBus, event);
	}
}
