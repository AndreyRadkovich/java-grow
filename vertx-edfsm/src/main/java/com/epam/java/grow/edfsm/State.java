package com.epam.java.grow.edfsm;

public abstract class State<T> {

	public final void doChangeState(T obj) {
		changeState(obj);
		synchronize(obj);
	}

	public abstract void changeState(T obj);

	public void synchronize(T obj) {

	}
}
