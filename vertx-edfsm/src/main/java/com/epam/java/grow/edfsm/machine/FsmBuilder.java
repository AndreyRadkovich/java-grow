package com.epam.java.grow.edfsm.machine;

import com.epam.java.grow.edfsm.State;
import io.vertx.core.eventbus.EventBus;

public class FsmBuilder<T> {

	private EventBus eventBus;

	public FsmBuilder(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	public FromWord<T> from(State<T> state) {
		return new FromWord<>(eventBus);
	}

}
