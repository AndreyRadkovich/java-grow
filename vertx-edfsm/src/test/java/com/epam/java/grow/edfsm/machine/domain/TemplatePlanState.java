package com.epam.java.grow.edfsm.machine.domain;

import com.epam.java.grow.edfsm.State;

public class TemplatePlanState {

	public static final State<TemplatePlan> CREATED = new State<TemplatePlan>() {
		@Override
		public void changeState(TemplatePlan obj) {
			obj.setStatus("CREATED");
		}

		@Override
		public void synchronize(TemplatePlan obj) {

		}
	};

	public static final State<TemplatePlan> APPROVED = new State<TemplatePlan>() {
		@Override
		public void changeState(TemplatePlan obj) {
			obj.setStatus("APPROVED");
		}
	};

	public static final State<TemplatePlan> VALIDATED = new State<TemplatePlan>() {
		@Override
		public void changeState(TemplatePlan obj) {
			obj.setStatus("VALIDATED");
		}
	};
}
