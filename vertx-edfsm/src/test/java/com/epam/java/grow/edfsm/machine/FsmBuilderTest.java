package com.epam.java.grow.edfsm.machine;

import com.epam.java.grow.edfsm.FiniteStateMachine;
import com.epam.java.grow.edfsm.machine.domain.TemplatePlan;
import org.junit.Test;

import static com.epam.java.grow.edfsm.machine.domain.TemplatePlanState.*;

public class FsmBuilderTest {

	@Test
	public void testFsmBuilder() throws Exception {

		FiniteStateMachine<TemplatePlan> finiteStateMachine = new FsmBuilder<TemplatePlan>(null).
				from(CREATED).
				  on("event1").goTo(APPROVED).
				  on("event2").perform(System.out::println).goTo(VALIDATED).
				from(APPROVED).
				  on("event3").perform(System.out::println).goTo(VALIDATED).
				build();

		finiteStateMachine.watch(new TemplatePlan());
	}
}
