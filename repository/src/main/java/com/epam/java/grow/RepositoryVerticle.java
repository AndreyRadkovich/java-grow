package com.epam.java.grow;

import io.vertx.core.*;
import io.vertx.core.eventbus.*;
import io.vertx.core.json.*;
import io.vertx.ext.mongo.*;

public class RepositoryVerticle extends AbstractVerticle {

  private MongoClient client;

  public RepositoryVerticle(Vertx vertx, MongoClient client) {
    this.vertx = vertx;
    this.client = client;
  }

  public static void main(String[] args) throws Exception {
    VertxOptions vertxOptions = new VertxOptions();
    Vertx.clusteredVertx(vertxOptions, res -> {
      if (res.succeeded()) {
        Vertx vertx = res.result();
        new RepositoryVerticle(vertx, MongoClient.createShared(vertx, new JsonObject(), "MongoRepositoryPool")).start();
      } else {
        System.out.println("Failed: " + res.cause());
      }
    });
  }

  @Override
  public void start() {
    EventBus eventBus = vertx.eventBus();
    eventBus
        .consumer("root")
        .handler(message -> {
          System.out.println("message = " + message);
          client.insert("events", new JsonObject().put("event", message.body()), res -> {
            if (res.succeeded()) {
              System.out.println("event has been logged into mongodb");
            } else {
              res.cause().printStackTrace();
            }
          });
        });
  }
}
