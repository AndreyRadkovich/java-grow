package com.epam.java.grow.web.routes;

import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

public class MainRoute {

	public static void handleGet(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
		response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		response.end("hello");
	}
}
