package com.epam.java.grow.web;

import com.epam.java.grow.web.routes.*;
import io.vertx.core.*;
import io.vertx.core.eventbus.*;
import io.vertx.ext.web.*;
import io.vertx.ext.web.handler.sockjs.*;

public class HttpServer extends AbstractVerticle {

  public static void main(String[] args) throws Exception {
    new HttpServer().start();
  }

  @Override
  public void start() throws Exception {
    this.vertx = Vertx.vertx();
    EventBus eventBus = vertx.eventBus();

    Router router = Router.router(vertx);

    SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
    PermittedOptions inbound = new PermittedOptions()
        .setAddress("input-area-changed");
    PermittedOptions outBound = new PermittedOptions()
        .setAddress("input-area-changed-from-other-browser");
    sockJSHandler.bridge(new BridgeOptions()
        .addInboundPermitted(inbound)
        .addOutboundPermitted(outBound)
    );

    eventBus.consumer("input-area-changed", message -> {
      System.out.println("message = " + message.body());
      eventBus.publish("input-area-changed-from-other-browser", message.body());
    });

    router.route("/eventbus/*").handler(sockJSHandler);

    router.get("/").handler(MainRoute :: handleGet);
    router.get("/home").handler(MainRoute :: handleGet);

    vertx.createHttpServer().requestHandler(router::accept).listen(8080);
  }
}
